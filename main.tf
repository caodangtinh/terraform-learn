# provider "aws" {
#   region     = "ap-southeast-1"
#   access_key = "AKIATVQDF7W5CY6LMO5Q"
#   secret_key = "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"
# }
provider "aws" {
    # credentials load from environment variable
}

variable "cidr_blocks" {
  type = list(object({
    name = string
    cidr_block = string
  }))
  description = "CIDR blocks and name tag for VPC and Subnet"
}

variable "vpc_cidr_block" {
  type = string
  default = "10.0.0.0/16"
  description = "CIDR block for VPC"
}

variable "avail_zone" {
  type = string
  description = "Availability Zone"
}
resource "aws_vpc" "development-vpc" {
    cidr_block = var.cidr_blocks[0].cidr_block
    tags = {
        Name = var.cidr_blocks[0].name
    }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    # cidr_block = "10.0.10.0/24"
    cidr_block = var.cidr_blocks[1].cidr_block
    # availability_zone = "ap-southeast-1a"
    availability_zone = var.avail_zone
    tags = {
        Name = var.cidr_blocks[1].name
    }
}

# data "aws_vpc" "existing_vpc" {
#     default = true
# }
 
# resource "aws_subnet" "dev-subnet-2" {
#     vpc_id = data.aws_vpc.existing_vpc.id
#     cidr_block = "172.31.48.0/20"
#     availability_zone = "ap-southeast-1a"
#     tags = {
#         Name = "subnet-2-dev"
#     }
# }

output "dev-vpc-id" {
  value = aws_vpc.development-vpc.id
}

output "dev-subnet-id" {
    value = aws_subnet.dev-subnet-1.id
}